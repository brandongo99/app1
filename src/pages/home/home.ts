import { Component } from '@angular/core';
import { NavController, AlertController, ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  juegos = '';
  constructor(public navCtrl: NavController, public msj: AlertController, public toast: ToastController) {

  }

  Mensaje(){
    console.log("Hola");
    let mensaje = this.msj.create({
      title: 'Bienvenidos Alumnos de BM',
      message: 'Hola Compañeros',
      buttons: ['Aceptar'],
    });
    mensaje.present();
  }

  goToPremium() {
    let t = this.toast.create({
      message: 'Esta función solo está disponible para los usuarios registrados.',
      duration: 2500,
      position: 'buttom',
      showCloseButton: true,
      closeButtonText: 'x',
    });
    t.present();
  }
}
